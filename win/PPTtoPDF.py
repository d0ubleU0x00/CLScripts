import os
import glob
from win32com.client import gencache
def GetFilePath():
    #obtain the path of all ppt/pptx files in this directory
    filePath=os.path.split(os.path.abspath(__file__))[0]
    pptFiles=glob.glob(os.path.join(filePath,"*.ppt*"))
    #print(pptFiles)
    return filePath,pptFiles 

def pptToPDF(filename,new_directory):
    #Example Initialize a PowerPoint application instance
    pdfName=os.path.basename(filename).split('.ppt')[0]+'.pdf'
    savePathFile=os.path.join(new_directory,pdfName)
    if os.path.isfile(savePathFile): #judge if the str savePathFile is a file existed
        print(pdfName,"converted already")
        return
    p=gencache.EnsureDispatch("PowerPoint.Application")# guessing that has some connections with open the PowerPoint Application
    try:
        ppt=p.Presentations.Open(filename,False,False,False)#open the PowerPoint file 
    except Exception as e:
        print(os.path.split(filename)[1],"File format conversion failed,because %s" %e)  #throw 
    ppt.ExportAsFixedFormat(savePathFile,2,PrintRange=None)
    print("converted && saved :", savePathFile)
    p.Quit     #close the PowerPoint file       

def main():
    filePath,pptFlies=GetFilePath();#PPT文件所在目录和所有ppt文件目录的列表
    NewDirectory=os.path.join(filePath,"converted_directory")
    #生成PDF文件的存储路径
    if not os.path.exists(NewDirectory):
        os.mkdir(os.path.join(NewDirectory))#crate the directory if it not exits
    for each in pptFlies:
        pptToPDF(each,NewDirectory)

if __name__ =="__main__":
    main()            
